# People's Policy Project Bot
Sends the articles from peoplespolicyproject.org to a telegram channel.

## Environment Variables:
- `BOT_TOKEN`: Token from botfather.
- `CHANNEL_ID`: Id from the channel where the messages should go.
- `RHASH_PPP`: Optional, rhash of the instant view that should be used instead.
- `UPDATE_TIME`: Optional, time between each update.

## Requirements:
- python3.8
- libsqlite3-dev
- `pip3 install -r requirements.txt`


## Run:
1. Install requirements
2. `mkdir volumes`
3. `BOT_TOKEN=YOURTOKEN CHANNEL_ID=YOURCHANNELID ./run`
