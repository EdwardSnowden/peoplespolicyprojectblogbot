from typing import Iterator, Optional
from logging import info, warning
import time
from dataclasses import dataclass
import requests
import re
from bs4 import BeautifulSoup
from peewee import CharField, Model
import telegram
from telegram import Update
from telegram.ext import Updater, MessageHandler, Filters, CallbackContext, CommandHandler
import html

from .globals import tg_channel_id, db, rhash_ppp, updater

PPP_PROJECTS_URL = "https://www.peoplespolicyproject.org/category/projects/"
PPP_PROJECTS_PAGED_URL = "https://www.peoplespolicyproject.org/category/projects/page/{}/"
PPP_POSTS_URL = "https://www.peoplespolicyproject.org/category/posts/"
PPP_POSTS_PAGED_URL = "https://www.peoplespolicyproject.org/category/posts/page/{}/"


# Database
class ProjectDB(Model):
    id = CharField(primary_key=True)

    class Meta:
        database = db


# Dataclasses
@dataclass
class Project:
    id: str  # link
    header: str
    short: str
    link: str
    date: str
    topic: str
    topic_url: str
    author: str
    author_url: str
    full_report_text: Optional[str]
    full_report_url: Optional[str]
    db_object: ProjectDB


def parse_ppp(articles) -> Iterator[Project]:
    for article in articles:
        article_id = article.a['href']
        if ProjectDB.get_or_none(ProjectDB.id == article_id):
            continue
        link = article_id
        content = article.find("div", {"class": "text"})
        header = content.h1.text.strip()
        short = content.div.text.strip()
        date = content.time['datetime']
        info("Parsing project: %s", header)
        db_obj = ProjectDB(id=article_id)
        # sub page
        sub_page = requests.get(link)
        sub_soup = BeautifulSoup(sub_page.content, 'html.parser')
        topic_tmp = sub_soup.find("p", {"class": "article-subject"}).a
        if topic_tmp:
            topic = topic_tmp.text.strip()
            topic_url = topic_tmp['href']
        else:
            topic = "None"
            topic_url = "#"
        author_tmp = sub_soup.find("figcaption", {"class": "author"})
        author = author_tmp.a.text.strip()
        author_url = author_tmp.a['href']
        full_report_tmp = sub_soup.find("figure", {"class": "project-download-primary"})
        if full_report_tmp:
            full_report_inner_tmp = full_report_tmp.find("a", {"class": "cta-link"})
            full_report_text = full_report_inner_tmp.text.strip()
            full_report_url = full_report_inner_tmp['href']
        else:
            full_report_text = None
            full_report_url = None
        # return
        yield Project(
            id=article_id, header=header, short=short, link=link, date=date,
            topic=topic, topic_url=topic_url, author=author, author_url=author_url,
            full_report_text=full_report_text, full_report_url=full_report_url,
            db_object=db_obj
        )


# Parsers
def parse_ppp_projects(page_num=1) -> Iterator[Project]:
    """Parses the ppp projects page and returns all new articles."""
    if page_num < 1:
        warning("Wrong page number.")
        return []
    if page_num == 1:
        page = requests.get(PPP_PROJECTS_URL)
    else:
        page = requests.get(PPP_PROJECTS_PAGED_URL.format(page_num))
    soup = BeautifulSoup(page.content, 'html.parser')
    articles = soup.find_all("li", {"class": "post-single"})
    return parse_ppp(articles)


# Parsers
def parse_ppp_posts(page_num=1) -> Iterator[Project]:
    """Parses the ppp posts page and returns all new articles."""
    if page_num < 1:
        warning("Wrong page number.")
        return []
    if page_num == 1:
        page = requests.get(PPP_POSTS_URL)
    else:
        page = requests.get(PPP_POSTS_PAGED_URL.format(page_num))
    soup = BeautifulSoup(page.content, 'html.parser')
    articles = soup.find_all("li", {"class": "post-single"})
    return parse_ppp(articles)


def wait_send(bot, article, save=True, **kwargs):
    try:
        message = bot.send_message(**kwargs)
    except telegram.error.RetryAfter as e:
        info(f"Sleeping {e.retry_after} seconds.")
        time.sleep(e.retry_after)
        return wait_send(bot, article, save=save, **kwargs)
    except telegram.error.TimedOut:
        info(f"Timed out, sleeping 5 seconds.")
        time.sleep(5)
        return wait_send(bot, article, save=save, **kwargs)
    if save:
        article.db_object.save(force_insert=True)
    return message


def send_articles(articles, bot, type):
    for article in reversed(list(articles)):
        preview_link = f"http://t.me/iv?url={article.link}&rhash={rhash_ppp}"
        text = f'<b>{article.header}</b><a href="{preview_link}"> </a>'
        text += f"\n<i>{type}</i> in #{article.topic.capitalize()}"
        text += f'\nBy <a href="{article.author_url}">{article.author}</a>'
        short = html.escape(article.short)
        text += f"\n\n{short}"
        text += f'\n<a href="{article.link}">Continue Reading</a>'
        info(f"Sending {article.header}.")
        save = article.full_report_text is None
        message = wait_send(bot, article, text=text, chat_id=tg_channel_id,
                            parse_mode="HTML", save=save)
        if article.full_report_text is not None:
            reply_text = f'<a href="{article.full_report_url}">{article.full_report_text}</a>'
            wait_send(bot, article, text=reply_text, chat_id=tg_channel_id,
                      reply_to_message_id=message.message_id, parse_mode="HTML",
                      save=True)


def update_ppp_projects(callback_context: CallbackContext):
    info("Updating ppp.")
    info("Parsing projects.")
    ppp_projects = parse_ppp_projects()
    info("Sending projects.")
    send_articles(ppp_projects, callback_context.bot, "Project")
    info("Parsing posts.")
    ppp_posts = parse_ppp_posts()
    info("Sending posts.")
    send_articles(ppp_posts, callback_context.bot, "Article")


def send_backlog():
    info("Loading older articles.")
    for page_num in range(3, 0, -1):
        info(f"Loading page {page_num}")
        ppp_projects = parse_ppp_projects(page_num)
        send_articles(ppp_projects, updater.bot, "Project")
    for page_num in range(38, 0, -1):
        info(f"Loading page {page_num}")
        ppp_posts = parse_ppp_posts(page_num)
        send_articles(ppp_posts, updater.bot, "Article")
