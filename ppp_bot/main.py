import logging
from logging import info
from telegram import Update
from telegram.ext import MessageHandler, Filters, CallbackContext, CommandHandler

from .globals import db, updater, dispatcher, tg_channel_id, update_time
from .ppp import ProjectDB, update_ppp_projects, send_backlog


# Logging
logging.basicConfig(level=logging.INFO)


def db_setup():
    """Creates the database and tables."""
    db.connect()
    db.create_tables([ProjectDB])


def start(update: Update, _: CallbackContext) -> None:
    """Handles the command /start."""
    if update.message is not None:
        update.message.reply_text("Available commands: /help and /license.")


def help_cmd(update: Update, _: CallbackContext) -> None:
    """Handles the command /help."""
    if update.message is not None:
        update.message.reply_text("This bot is used to send the articles from peoplespolicyproject.org to @peoplespolicyproject.\n\nAvailable commands:\n/help\n/license")


def license_cmd(update: Update, _: CallbackContext) -> None:
    """Handles the command /license."""
    if update.message is not None:
        update.message.reply_text("This bot is licensed under [AGPL3](https://git.abfelbaum.dev/EdwardSnowden/peoplespolicyprojectblogbot/-/blob/main/LICENSE) and the code can be found [here](https://git.abfelbaum.dev/EdwardSnowden/peoplespolicyprojectblogbot)!", parse_mode="markdown")


def main():
    """Main method."""
    db_setup()
    # Standard Methods
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_cmd))
    dispatcher.add_handler(CommandHandler("license", license_cmd))
    updater.job_queue.run_repeating(update_ppp_projects, update_time)
    info("Start polling.")
    updater.start_polling()
    info("Idle.")
    updater.idle()
    info("Stop all.")


def backlog():
    """Sends all posts from the past."""
    db_setup()
    send_backlog()
