import os
from peewee import SqliteDatabase
from telegram.ext import Updater


# Database Connection
db = SqliteDatabase("volumes/database.db")


# Telegram Bot
updater = Updater(os.environ["BOT_TOKEN"], use_context=True)
dispatcher = updater.dispatcher
tg_channel_id = int(os.environ["CHANNEL_ID"])
rhash_ppp = os.environ.get("RHASH_PPP", "85c61fe47b9ab4")  # rhash of the instant view
update_time = int(os.environ.get("UPDATE_TIME", 600))
